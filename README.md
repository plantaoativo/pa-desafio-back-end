# Desafio - Back-End Developer

Sua tarefa é construir uma API e banco de dados para a aplicação CMS (Content Management System). A aplicação é um repositório para gerenciar postagens com seus respectivos titulos, autores, conteúdos e tags. Utilize um repositório Git (público, de preferência) para versionamento e disponibilização do código.

A aplicação pode ser construída utilizando [Laravel](https://laravel.com) (8 ou superior), utilizando um dos SGBDs definidos. 

A API deverá ser documentada utilizando o formato [API Blueprint](https://apiblueprint.org/) ou [Swagger](https://swagger.io/docs/specification/basic-structure/).

## O que será avaliado

Queremos avaliar sua capacidade de desenvolver e documentar um back-end para uma aplicação. Serão avaliados:

-   Código bem escrito e limpo;
-   Seu conhecimento em banco de dados, requisições HTTP, APIs REST, etc;
-   Sua capacidade de se comprometer com o que foi fornecido;
-   Sua capacidade de documentação da sua aplicação.

## O mínimo necessário

Os seguintes itens **são obrigatórios**, deverão estar presentes no projeto apresentado, para que o mesmo seja aprovado e passe para a próxima fase.

-   Uma aplicação contendo uma API real, que atenda os requisitos descritos abaixo, fazendo requisições à um banco de dados para persistência;
-   [README.md](http://README.md) contendo informações básicas do projeto e como executá-lo;
-   [API Blueprint](https://apiblueprint.org/) ou [Swagger](https://swagger.io/docs/specification/basic-structure/) da aplicação.
-   Dockerização da aplicação;
-   Criação de um banco de dados (**MySQL, PostgreSQL**)
-   Migrations para configuração do banco de dados utilizado;
-   Seeders para popular as tabelas criadas;
-   Cuidados especiais com otimização, padrões, entre outros;
-   Autenticação e autorização (**Passport, JWT**);
-   Trait para Requests;
-   Trait para Response;
-   Dependency Injection;
-   Limitação de requisições para a API (**Rate Limiting**);
-   Utilização de paginação nas respostas da API para métodos GET;
-   Repository Patern;

## Bônus

Os seguintes itens **não são obrigatórios**, adicione um ou mais e darão mais valor ao seu trabalho (os em negrito são mais significativos para nós e serão considerados um diferencial)

-   Uso de ferramentas externas que facilitem o seu trabalho;
-   **Testes**;
-   **Contracts**;
-   **Intarfaces**;
-   **Pipelines de CI/CD (GitLab Actions, GitHub Actions, Jenkinsfile, etc);**
-   **Deploy em ambientes reais como AWS, Heroku, GCP etc;**
-   Sugestões sobre o challenge embasadas em alguma argumentação.

# Requisitos

# Rotas sem autenticação

## 1: Deve haver uma rota para autenticar um usuário com email e senha

Corpo da requisição:

`POST /auth/login Content-Type: application/json`

```js
{
    "email": "joao.silva@example.com",
    "password": "senha123"
}
```

Resposta:


`Content-Type: application/json`

```js
{
    "message": "Login realizado com sucesso.",
    "user": {
        "id": 1,
        "nome": "João Silva",
        "email": "joao.silva@example.com",
        "telefone": "(11) 98765-4321",
        "is_valid": true
    },
    "token": "seu-token-jwt-aqui"
}
```
Resposta em caso de erro:

```js
{
    "message": "Credenciais inválidas. Verifique seu email e senha."
}
```

## 2: Deve haver uma rota para registrar um novo usuário (caso o usuário ainda não exista)

Corpo da requisição:

`POST /auth/register Content-Type: application/json`

```js
{
    "nome": "Ana Costa",
    "email": "ana.costa@example.com",
    "password": "senha123",
    "telefone": "(41) 99877-6655"
}
```

Resposta:

`Content-Type: application/json`

```js
{
    "message": "Usuário registrado com sucesso.",
    "user": {
        "id": 4,
        "nome": "Ana Costa",
        "email": "ana.costa@example.com",
        "telefone": "(41) 99877-6655",
        "is_valid": true
    },
    "token": "seu-token-jwt-aqui"
}
```
Resposta em caso de erro:

```js
{
     "message": "Erro ao cadastrar o usuário. Verifique os dados fornecidos."
}
```


# Rotas protegidas, somente usuários autenticados conseguem requisitar.

## 1: Deve haver uma rota para fazer o logout de um usuário

`POST /auth/logout`

Resposta em caso de sucesso:

```js
{
    "message": "Logout realizado com sucesso."
}
```

# Usuários

## 1: Deve haver uma rota para listar todos os usuários cadastrados

`GET /users`

Resposta:

```js
[
    {
        "id": 1,
        "nome": "João Silva",
        "email": "joao.silva@example.com",
        "telefone": "(11) 98765-4321",
        "is_valid": true
    },
    {
        "id": 2,
        "nome": "Maria Oliveira",
        "email": "maria.oliveira@example.com",
        "telefone": "(21) 91234-5678",
        "is_valid": false
    },
    {
        "id": 3,
        "nome": "Pedro Souza",
        "email": "pedro.souza@example.com",
        "telefone": "(31) 99876-5432",
        "is_valid": true
    }
]
```

## 2: Deve haver uma rota para cadastrar um novo usuário

O corpo da requisição deve conter as informações do usuário a ser cadastrado, sem o ID (gerado automaticamente pelo servidor). A resposta, em caso de sucesso, deve ser o mesmo objeto, com seu novo ID gerado.

`POST /users Content-Type: application/json`

```js
{
    "nome": "Ana Costa",
    "email": "ana.costa@example.com",
    "password": "senha123",
    "telefone": "(41) 99877-6655",
    "is_valid": true
}
```

Resposta:

`Content-Type: application/json`

```js
{
    "nome": "Ana Costa",
    "email": "ana.costa@example.com",
    "password": "senha123",
    "telefone": "(41) 99877-6655",
    "is_valid": true,
    "id": 4 // ou qualquer outro identificador
}
```

## 3: Deve haver uma rota para editar um usuário

O corpo da requisição deve conter as informações editadas do usuário, sem o ID (informado na URL da requisição). Deve ser possível editar os valores individualmente (`nome`, `email`, `password`, `telefone`, `is_valid`), ou todos os valores de uma vez só. A resposta, em caso de sucesso, deve ser o mesmo objeto, com os valores alterados.

`PUT /users/{id} Content-Type: application/json`

```js
{
    "nome": "Ana Costa",
    "email": "ana.costa@example.com",
    "password": "novaSenha456",
    "telefone": "(41) 99877-6655",
    "is_valid": false
}
```

Resposta:

`Content-Type: application/json`

```js
{
    "nome": "Ana Costa",
    "email": "ana.costa@example.com",
    "password": "novaSenha456",
    "telefone": "(41) 99877-6655",
    "is_valid": false,
    "id": 4 // ou qualquer outro identificador
}
```

## 4: O usuário deve poder remover um usuário por ID

`DELETE /users/{id}`

Resposta:


# Posts

## 1: Deve haver uma rota para listar todas as postagens cadastradas

`GET /posts`

Resposta:

```js
[
    {
        "id": 1, // ou qualquer outro identificador
        "title": "Notion",
        "author": {
            "id": 1,
            "nome": "Nome do Usuario",
            "telefone": "0000000000"
            "email": "email@email.com"
        },
        "content": "Sed soluta nemo et consectetur reprehenderit ea reprehenderit sit. Aut voluptate sit omnis qui repudiandae. Cum sit provident eligendi tenetur facere ut quo. Commodi voluptate ut aut deleniti.",
        "tags": [
            "organization",
            "planning",
            "collaboration",
            "writing",
            "calendar"
        ]
    },
    {
        "id": 2,
        "title": "json-server",
        "author": {
            "id": 1,
            "nome": "Nome do Usuario",
            "telefone": "0000000000"
            "email": "email@email.com"
        },
        "content": "Laudantium illum modi tenetur possimus natus. Sed tempora molestiae fugiat id dolor rem ea aliquam. Ipsam quibusdam quam consequuntur. Quis aliquid non enim voluptatem nobis. Error nostrum assumenda ullam error eveniet. Ut molestiae sit non suscipit.\nQui et eveniet vel. Tenetur nobis alias dicta est aut quas itaque non. Omnis iusto architecto commodi molestiae est sit vel modi. Necessitatibus voluptate accusamus.",
        "tags": [
            "api",
            "json",
            "schema",
            "node",
            "github",
            "rest"
        ]
    },
    {
        "id": 3,
        "title": "fastify",
        "author": {
            "id": 1,
            "nome": "Nome do Usuario",
            "telefone": "0000000000"
            "email": "email@email.com"
        },
        "content": "Eos corrupti qui omnis error repellendus commodi praesentium necessitatibus alias. Omnis omnis in. Labore aut ea minus cumque molestias aut autem ullam. Consectetur et labore odio quae eos eligendi sit. Quam placeat repellendus.\n Odio nisi dolores dolorem ea. Qui dicta nulla eos quidem iusto. Voluptatibus qui est accusamus sint perferendis est quae recusandae. Qui repudiandae cupiditate fugiat est.",
        "tags": [
            "web",
            "framework",
            "node",
            "http2",
            "https",
            "localhost"
        ]
    }
]

```

## 2: Deve ser possível filtrar postagens utilizando uma busca por tag

`GET /posts?tag=node` (_node_ é a tag sendo buscada neste exemplo)

Resposta:

```js
[
    {
        "id": 2,
        "title": "json-server",
        "author": {
            "id": 1,
            "nome": "Nome do Usuario",
            "telefone": "0000000000"
            "email": "email@email.com"
        },
        "content": "Laudantium illum modi tenetur possimus natus. Sed tempora molestiae fugiat id dolor rem ea aliquam. Ipsam quibusdam quam consequuntur. Quis aliquid non enim voluptatem nobis. Error nostrum assumenda ullam error eveniet. Ut molestiae sit non suscipit.\nQui et eveniet vel. Tenetur nobis alias dicta est aut quas itaque non. Omnis iusto architecto commodi molestiae est sit vel modi. Necessitatibus voluptate accusamus.",
        "tags": [
            "api",
            "json",
            "schema",
            "node",
            "github",
            "rest"
        ]
    },
    {
        "id": 3,
        "title": "fastify",
        "author": {
            "id": 1,
            "nome": "Nome do Usuario",
            "telefone": "0000000000"
            "email": "email@email.com"
        },
        "content": "Eos corrupti qui omnis error repellendus commodi praesentium necessitatibus alias. Omnis omnis in. Labore aut ea minus cumque molestias aut autem ullam. Consectetur et labore odio quae eos eligendi sit. Quam placeat repellendus.\n Odio nisi dolores dolorem ea. Qui dicta nulla eos quidem iusto. Voluptatibus qui est accusamus sint perferendis est quae recusandae. Qui repudiandae cupiditate fugiat est.",
        "tags": [
            "web",
            "framework",
            "node",
            "http2",
            "https",
            "localhost"
        ]
    }
]

```

## 3: Deve haver uma rota para pesquisar postagens pelo title ou content

`GET /posts?query=palavra-chave`

Corpo da requisição: (não há corpo, a busca é realizada com base na query string)

Resposta:

```js
[
    {
        "id": 1,
        "title": "Notion",
        "author": "Marcia Thiel",
        "content": "Sed soluta nemo et consectetur reprehenderit ea reprehenderit sit. Aut voluptate sit omnis qui repudiandae. Cum sit provident eligendi tenetur facere ut quo. Commodi voluptate ut aut deleniti.",
        "tags": [
            "organization",
            "planning",
            "collaboration",
            "writing",
            "calendar"
        ]
    }
]
```

## 4: Deve haver uma rota para cadastrar uma nova postagem

O corpo da requisição deve conter as informações da postagem a ser cadastrada, sem o ID (gerado automaticamente pelo servidor). A resposta, em caso de sucesso, deve ser o mesmo objeto, com seu novo ID gerado.

`POST /posts Content-Type: application/json`

```js
{
    "title": "hotel",
    "author": 1,
    "content": "Local app manager. Start apps within your browser, developer tool with local .localhost domain and https out of the box.",
    "tags":["node", "organizing", "webapps", "domain", "developer", "https", "proxy"]
}

```

Resposta:


`Content-Type: application/json`

```js
{
    "title": "hotel",
    "author": {
            "id": 1,
            "nome": "Nome do Usuario",
            "telefone": "0000000000"
            "email": "email@email.com"
        },
    "content": "Local app manager. Start apps within your browser, developer tool with local .localhost domain and https out of the box.",
    "tags":["node", "organizing", "webapps", "domain", "developer", "https", "proxy"],
    "id":5 // ou qualquer outro identificador
}

```
## 5: Deve haver uma rota para editar uma nova postagem

O corpo da requisição deve conter as informações editadas da postagem, sem o ID (informado na URL da requisição). Deve ser possível editar os valores individualmente (`title`, `authot`, `content` ou `tags`), ou todos os valores de uma vez só.  A resposta, em caso de sucesso, deve ser o mesmo objeto, com os valores alterados.

`PUT /posts/{id} Content-Type: application/json`

```js
{
    "title": "hotel",
    "author": 2,
    "content": "Local app manager. Start apps within your browser, developer tool with local .localhost domain and https out of the box.",
    "tags":["organizing", "webapps", "domain", "developer", "proxy"]
}

```

Resposta:


`Content-Type: application/json`

```js
{
    "title": "hotel",
    "author": {
            "id": 2,
            "nome": "Nome do Usuario",
            "telefone": "0000000000"
            "email": "email@email.com"
        },
    "content": "Local app manager. Start apps within your browser, developer tool with local .localhost domain and https out of the box.",
    "tags":["organizing", "webapps", "domain", "developer", "proxy"],
    "id":5 // ou qualquer outro identificador
}

```

## 6: O usuário deve poder remover uma postagem por ID

`DELETE /posts/{id}`

Resposta:


## Critérios de Aceitação

-   A API deve ser real e escrita por você. Não serão aceitas APIs escritas de outros autores ou ferramentas que criam APIs automaticamente (Loopback, json-server, etc);
-   Todos os requisitos acima devem ser cumpridos, seguindo o padrão de rotas estabelecido;
-   Deve haver um documento de API Blueprint ou OpenAPI (antigo Swagger) descrevendo sua API;
-   Se você julgar necessário, adequado ou quiser deixar a aplicação mais completa (bônus!) você pode adicionar outras rotas, métodos, meios de autenticação com usuários, etc.